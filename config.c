#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "profiler.h"


static struct config_counter default_counters[] = {
    { "ARITH:DIVIDER_ACTIVE", 2000003 },
    { "BACLEARS:ANY", 100003 },
    { "BR_INST_RETIRED:ALL_BRANCHES", 400009 },
    { "BR_INST_RETIRED:NEAR_CALL", 100007 },
    { "BR_INST_RETIRED:NEAR_TAKEN", 400009 },
    { "BR_MISP_RETIRED:ALL_BRANCHES", 400009 },
    { "CPU_CLK_THREAD_UNHALTED:ONE_THREAD_ACTIVE", 2000003 },
    { "CPU_CLK_THREAD_UNHALTED:REF_XCLK_ANY", 2503 },
    { "CPU_CLK_UNHALTED:ONE_THREAD_ACTIVE", 2503 },
    { "CPU_CLK_UNHALTED", 2000003 },
    { "CPU_CLK_UNHALTED:REF_XCLK", 2503 },
    { "CPU_CLK_UNHALTED:THREAD_P", 2000003 },
    { "CYCLE_ACTIVITY:STALLS_L1D_MISS", 2000003 },
    { "CYCLE_ACTIVITY:STALLS_L2_MISS", 2000003 },
    { "CYCLE_ACTIVITY:STALLS_L3_MISS", 2000003 },
    { "CYCLE_ACTIVITY:STALLS_MEM_ANY", 2000003 },
    { "DTLB_LOAD_MISSES:STLB_HIT", 2000003 },
    { "DTLB_LOAD_MISSES:WALK_ACTIVE", 100003 },
    { "DTLB_LOAD_MISSES:WALK_PENDING", 2000003 },
    { "DTLB_STORE_MISSES:WALK_PENDING", 2000003 },
    { "EPT:WALK_PENDING", 2000003 },
    { "EXE_ACTIVITY:1_PORTS_UTIL", 2000003 },
    { "EXE_ACTIVITY:2_PORTS_UTIL", 2000003 },
    { "EXE_ACTIVITY:BOUND_ON_STORES", 2000003 },
    { "EXE_ACTIVITY:EXE_BOUND_0_PORTS", 2000003 },
    { "FP_ARITH_INST_RETIRED:128B_PACKED_DOUBLE", 2000003 },
    { "FP_ARITH_INST_RETIRED:128B_PACKED_SINGLE", 2000003 },
    { "FP_ARITH_INST_RETIRED:256B_PACKED_DOUBLE", 2000003 },
    { "FP_ARITH_INST_RETIRED:256B_PACKED_SINGLE", 2000003 },
    { "FP_ARITH_INST_RETIRED:SCALAR_DOUBLE", 2000003 },
    { "FP_ARITH_INST_RETIRED:SCALAR_SINGLE", 2000003 },
    { "ICACHE_16B:IFDATA_STALL", 2000003 },
    { "ICACHE_64B:IFTAG_HIT", 200003 },
    { "ICACHE_64B:IFTAG_MISS", 200003 },
    { "ICACHE_64B:IFTAG_STALL", 200003 },
    { "IDQ:DSB_UOPS", 2000003 },
    { "IDQ:MITE_UOPS", 2000003 },
    { "IDQ:MS_SWITCHES", 2000003 },
    { "IDQ:MS_UOPS", 2000003 },
    { "IDQ_UOPS_NOT_DELIVERED:CORE", 2000003 },
    { "IDQ_UOPS_NOT_DELIVERED:CYCLES_0_UOPS_DELIV_CORE", 2000003 },
    { "INST_RETIRED:ANY_P", 2000003 },
    { "INT_MISC:CLEAR_RESTEER_CYCLES", 2000003 },
    { "INT_MISC:RECOVERY_CYCLES", 2000003 },
    { "INT_MISC:RECOVERY_CYCLES_ANY", 2000003 },
    { "ITLB_MISSES:STLB_HIT", 100003 },
    { "ITLB_MISSES:WALK_PENDING", 100003 },
    { "L1D_PEND_MISS:PENDING", 2000003 },
    { "L1D_PEND_MISS:PENDING_CYCLES", 2000003 },
    { "L1D:REPLACEMENT", 2000003 },
    { "L2_LINES_IN:ALL", 100003 },
    { "LSD:UOPS", 2000003 },
    { "MACHINE_CLEARS:COUNT", 100003 },
    { "MEM_INST_RETIRED:ALL_LOADS", 2000003 },
    { "MEM_INST_RETIRED:ALL_STORES", 2000003 },
    { "MEM_LOAD_L3_HIT_RETIRED:XSNP_HITM", 20011 },
    { "MEM_LOAD_L3_HIT_RETIRED:XSNP_HIT", 20011 },
    { "MEM_LOAD_L3_HIT_RETIRED:XSNP_MISS", 20011 },
    { "MEM_LOAD_RETIRED:FB_HIT", 100007 },
    { "MEM_LOAD_RETIRED:L1_MISS", 100003 },
    { "MEM_LOAD_RETIRED:L2_HIT", 100003 },
    { "MEM_LOAD_RETIRED:L2_MISS", 50021 },
    { "MEM_LOAD_RETIRED:L3_HIT", 50021 },
    { "MEM_LOAD_RETIRED:L3_MISS", 100007 },
    { "OFFCORE_REQUESTS_OUTSTANDING:ALL_DATA_RD_CYCLES", 2000003 },
    { "UOPS_EXECUTED:CORE_CYCLES_GE_1", 2000003 },
    { "UOPS_EXECUTED:THREAD", 2000003 },
    { "UOPS_ISSUED:ANY", 2000003 },
    { "UOPS_RETIRED:RETIRE_SLOTS", 2000003 },
    { "PARTIAL_RAT_STALLS:SCOREBOARD", 2000003 },
    { "ICACHE_16B:IFDATA_STALL:C=1:E=1", 2000003 },
    { "L1D_PEND_MISS:FB_FULL:C=1", 2000003 },
    { "OFFCORE_REQUESTS_OUTSTANDING:ALL_DATA_RD", 2000003 },
};


struct config_counter *config_counters = default_counters;
size_t config_counter_num = sizeof(default_counters) / sizeof(default_counters[0]);

struct config_process *config_procs = NULL;
size_t config_procs_num = 0;

struct config_thread *config_threads = NULL;
size_t config_threads_num = 0;

uint64_t config_duration = 0;

static int is_pid(const char *dname)
{
  size_t i;
  for (i = 0; isdigit(dname[i]); i++);
  return (dname[i] == 0);
}

static int add_thread(struct config_process *p, pid_t tid, const char *comm)
{
  struct config_thread *t;

  if ((t = calloc(1, sizeof(*t))) == NULL) {
    perror("add_thread: calloc failed");
  }

  t->tid = tid;
  t->comm = strdup(comm);
  t->proc = p;

  t->next = config_threads;
  config_threads = t;
  config_threads_num++;

  t->next_proc = p->threads;
  p->threads = t;
  p->num_threads++;

  return 0;
}

static int add_process(pid_t pid, const char *comm)
{
  DIR *d;
  struct dirent *de;
  char path[64];
  char buf[65];
  int fd;
  ssize_t ret;
  pid_t tid;
  struct config_process *p;

  if ((p = calloc(1, sizeof(*p))) == NULL) {
    perror("add_process: calloc failed");
    return -1;
  }

  p->pid = pid;
  p->comm = strdup(comm);
  p->threads = NULL;
  p->num_threads = 0;
  p->next = config_procs;
  config_procs = p;
  config_procs_num++;

  snprintf(path, sizeof(path), "/proc/%u/task", pid);

  if ((d = opendir(path)) == NULL) {
    perror("opening proc failed");
    return -1;
  }

  while ((de = readdir(d)) != NULL) {
    if (de->d_type != DT_DIR || !is_pid(de->d_name))
      continue;

    tid = strtoul(de->d_name, NULL, 10);

    snprintf(path, sizeof(path), "/proc/%u/comm", tid);
    if ((fd = open(path, O_RDONLY)) < 0) {
      perror("open proc/comm failed");
      return -1;
    }

    if ((ret = read(fd, buf, sizeof(buf) - 1)) <= 0) {
      perror("read proc/comm failed");
      return -1;
    }
    buf[ret - 1] = 0;
    close(fd);

    add_thread(p, tid, buf);
  }

  closedir(d);
  return 0;
}

static int find_processes(const char *name)
{
  DIR *d;
  struct dirent *de;
  static char path[512];
  char buf[65];
  int fd, cnt;
  ssize_t ret;
  pid_t pid;

  if ((d = opendir("/proc")) == NULL) {
    perror("opening proc failed");
    return -1;
  }

  cnt = 0;
  while ((de = readdir(d)) != NULL) {
    if (de->d_type != DT_DIR || !is_pid(de->d_name))
      continue;

    snprintf(path, sizeof(path), "/proc/%s/comm", de->d_name);
    if ((fd = open(path, O_RDONLY)) < 0) {
      perror("open proc/comm failed");
      return -1;
    }

    if ((ret = read(fd, buf, sizeof(buf) - 1)) <= 0) {
      perror("read proc/comm failed");
      return -1;
    }
    buf[ret - 1] = 0;
    close(fd);

    if (strcmp(buf, name))
      continue;

    pid = strtoul(de->d_name, NULL, 10);

    add_process(pid, buf);
    cnt++;
  }

  closedir(d);
  return cnt;
}


int config_init(int argc, char *argv[])
{
  int c, opt_idx, done;
  static struct option long_opts[] = {
      {"process", required_argument, NULL, 'p'},
      {"duration", required_argument, NULL, 'd'},
      {NULL, 0, NULL, 0},
    };
  static const char *short_opts = "p:d:";

  done = 0;
  while (!done) {
    c = getopt_long(argc, argv, short_opts, long_opts, &opt_idx);
    switch (c) {
      case 'p':
        find_processes(optarg);
        break;

      case 'd':
        config_duration = strtoul(optarg, NULL, 10);
        break;

      case -1:
        done = 1;
        break;

      case '?':
        break;

      default:
        abort();
    }
  }
  return 0;
}
