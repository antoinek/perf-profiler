from profiling import *

class TopDown(SGAnalysis):
    smt = False

    #
    # L1
    #
    def get_l1_frontend_bound(self):
        return self.get('IDQ_UOPS_NOT_DELIVERED:CORE') / self.get('slots')

    def get_l1_bad_speculation(self):
        return (self.get('UOPS_ISSUED:ANY') - self.get('UOPS_RETIRED:RETIRE_SLOTS')
                + self.get('pipeline_width') * self.get('recovery_cycles')) / \
            self.get('slots')

    def get_l1_retiring(self):
        return self.get('UOPS_RETIRED:RETIRE_SLOTS') / self.get('slots')

    def get_l1_backend_bound(self):
        return 1 - (self.get('l1_frontend_bound') +
                self.get('l1_bad_speculation') +
                self.get('l1_retiring'))

    #
    # L2
    #
    def get_l2_frontend_latency(self):
        return self.get('pipeline_width') * \
            self.get('IDQ_UOPS_NOT_DELIVERED:CYCLES_0_UOPS_DELIV_CORE') / \
            self.get('slots')

    def get_l2_frontend_bandwidth(self):
        return self.get('l1_frontend_bound') - self.get('l2_frontend_latency')


    def get_l2_branch_mispredicts(self):
        return self.get('mispred_clears_fraction') * self.get('l1_bad_speculation')

    def get_l2_machine_clears(self):
        return self.get('l1_bad_speculation') - self.get('l2_branch_mispredicts')


    def get_l2_microcode_sequencer(self):
        return self.get('retire_uop_fraction') * self.get('IDQ:MS_UOPS') / self.get('slots')

    def get_l2_general_retirement(self):
        return self.get('l1_retiring') - self.get('l2_microcode_sequencer')


    def get_l2_memory_bound(self):
        return self.get('memory_bound_fraction') * self.get('l1_backend_bound')

    def get_l2_core_bound(self):
        return self.get('l1_backend_bound') - self.get('l2_memory_bound')


    #
    # Auxiliary
    #
    def get_slots(self):
        return self.get('pipeline_width') * self.get('core_clks')

    def get_pipeline_width(self):
        return 4

    def get_core_clks(self):
        if self.smt:
            raise 'TODO'
        return self.get('clks')

    def get_clks(self):
        return self.get('CPU_CLK_UNHALTED:THREAD_P')

    def get_ipc(self):
        return self.get('INST_RETIRED:ANY_P') / self.get('clks')

    def get_cpi(self):
        return self.div(1, self.get('ipc'))

    def get_recovery_cycles(self):
        if self.smt:
            return self.get('INT_MISC:RECOVERY_CYCLES_ANY') / 2
        return self.get('INT_MISC:RECOVERY_CYCLES_ANY')

    def get_memory_bound_fraction(self):
        return (self.get('CYCLE_ACTIVITY:STALLS_MEM_ANY') +
                self.get('EXE_ACTIVITY:BOUND_ON_STORES')) / \
                self.get('backend_bound_cycles')

    def get_backend_bound_cycles(self):
        return (self.get('EXE_ACTIVITY:EXE_BOUND_0_PORTS') +
                self.get('EXE_ACTIVITY:1_PORTS_UTIL') +
                self.get('few_uops_executed_threshold')) + \
            (self.get('CYCLE_ACTIVITY:STALLS_MEM_ANY') +
                self.get('EXE_ACTIVITY:BOUND_ON_STORES'))

    def get_few_uops_executed_threshold(self):
        if self.get('ipc') > 1.8:
            return self.get('EXE_ACTIVITY:2_PORTS_UTIL')
        return 0

    def get_retire_uop_fraction(self):
        return self.get('UOPS_RETIRED:RETIRE_SLOTS') / \
                self.get('UOPS_ISSUED:ANY')

    def get_mispred_clears_fraction(self):
        return self.get('BR_MISP_RETIRED:ALL_BRANCHES') / \
            (self.get('BR_MISP_RETIRED:ALL_BRANCHES') +
                self.get('MACHINE_CLEARS:COUNT'))



class TopDownAbs(SGAnalysis):
    smt = False

    #
    # L1
    #
    def get_l1_frontend_bound(self):
        return self.get('IDQ_UOPS_NOT_DELIVERED:CORE')

    def get_l1_bad_speculation(self):
        return (self.get('UOPS_ISSUED:ANY') - self.get('UOPS_RETIRED:RETIRE_SLOTS')
                + self.get('pipeline_width') * self.get('recovery_cycles'))

    def get_l1_retiring(self):
        return self.get('UOPS_RETIRED:RETIRE_SLOTS')

    def get_l1_backend_bound(self):
        return self.get('slots') - (self.get('l1_frontend_bound') +
                self.get('l1_bad_speculation') +
                self.get('l1_retiring'))

    #
    # L2
    #
    def get_frontend_latency(self):
        return self.get('pipeline_width') * \
            self.get('IDQ_UOPS_NOT_DELIVERED:CYCLES_0_UOPS_DELIV_CORE')

    def get_frontend_bandwidth(self):
        return self.get('l1_frontend_bound') - self.get('frontend_latency')


    def get_branch_mispredicts(self):
        return self.get('mispred_clears_fraction') * self.get('l1_bad_speculation')

    def get_machine_clears(self):
        return self.get('l1_bad_speculation') - self.get('branch_mispredicts')


    def get_microcode_sequencer(self):
        return self.get('retire_uop_fraction') * self.get('IDQ:MS_UOPS')

    def get_general_retirement(self):
        return self.get('l1_retiring') - self.get('microcode_sequencer')


    def get_memory_bound(self):
        return self.get('memory_bound_fraction') * self.get('l1_backend_bound')

    def get_core_bound(self):
        return self.get('l1_backend_bound') - self.get('memory_bound')


    #
    # Auxiliary
    #
    def get_slots(self):
        return self.get('pipeline_width') * self.get('core_clks')

    def get_pipeline_width(self):
        return 4

    def get_core_clks(self):
        if self.smt:
            raise 'TODO'
        return self.get('clks')

    def get_clks(self):
        return self.get('CPU_CLK_UNHALTED:THREAD_P')

    def get_ipc(self):
        return self.get('INST_RETIRED:ANY_P') / self.get('clks')

    def get_cpi(self):
        return self.div(1, self.get('ipc'))

    def get_recovery_cycles(self):
        if self.smt:
            return self.get('INT_MISC:RECOVERY_CYCLES_ANY') / 2
        return self.get('INT_MISC:RECOVERY_CYCLES_ANY')

    def get_memory_bound_fraction(self):
        return (self.get('CYCLE_ACTIVITY:STALLS_MEM_ANY') +
                self.get('EXE_ACTIVITY:BOUND_ON_STORES')) / \
                self.get('backend_bound_cycles')

    def get_backend_bound_cycles(self):
        return (self.get('EXE_ACTIVITY:EXE_BOUND_0_PORTS') +
                self.get('EXE_ACTIVITY:1_PORTS_UTIL') +
                self.get('few_uops_executed_threshold')) + \
            (self.get('CYCLE_ACTIVITY:STALLS_MEM_ANY') +
                self.get('EXE_ACTIVITY:BOUND_ON_STORES'))

    def get_few_uops_executed_threshold(self):
        if self.get('ipc') > 1.8:
            return self.get('EXE_ACTIVITY:2_PORTS_UTIL')
        return 0

    def get_retire_uop_fraction(self):
        return self.get('UOPS_RETIRED:RETIRE_SLOTS') / \
                self.get('UOPS_ISSUED:ANY')

    def get_mispred_clears_fraction(self):
        return self.get('BR_MISP_RETIRED:ALL_BRANCHES') / \
            (self.get('BR_MISP_RETIRED:ALL_BRANCHES') +
                self.get('MACHINE_CLEARS:COUNT'))
