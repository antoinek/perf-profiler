from profiling import *
from topdown import *
from report_utils import *
from splittcp import *
import sys

(md, sgs) = load_file(sys.argv[1])
sgs = sgs_filter(sgs, lambda a: a['symbol'] == sys.argv[2])
balance_counters(sgs)


total_sg = sgs_aggregate(sgs, lambda a: '')[0]

td = TopDown(total_sg)

print('IPC:                       %5.2f' % (td.get('ipc')))
print('Retiring:                  %5.2f%%' % (td.get('l1_retiring') * 100))
print('  General Retirement:              %5.2f%%' % (td.get('l2_general_retirement') * 100))
print('  Microcode Sequence:              %5.2f%%' % (td.get('l2_microcode_sequencer') * 100))
print('Frontend Bound:            %5.2f%%' % (td.get('l1_frontend_bound') * 100))
print('  Latency:                         %5.2f%%' % (td.get('l2_frontend_latency') * 100))
print('  Bandwidth:                       %5.2f%%' % (td.get('l2_frontend_bandwidth') * 100))
print('Backend Bound:             %5.2f%%' % (td.get('l1_backend_bound') * 100))
print('  Memory Bound:                    %5.2f%%' % (td.get('l2_memory_bound') * 100))
print('  Core Bound:                      %5.2f%%' % (td.get('l2_core_bound') * 100))
print('Bad Speculation:           %5.2f%%' % (td.get('l1_bad_speculation') * 100))
print('  Branch Mispredicts:              %5.2f%%' % (td.get('l2_branch_mispredicts') * 100))
print('  Machine Clears:                  %5.2f%%' % (td.get('l2_machine_clears') * 100))

print('\n\n\n')


td = TopDownAbs(total_sg)

print('Retiring:                  %15u' % (td.get('l1_retiring') / 4))
print('  General Retirement:              %15u' % (td.get('general_retirement') / 4))
print('  Microcode Sequence:              %15u' % (td.get('microcode_sequencer') / 4))
print('Frontend Bound:            %15u' % (td.get('l1_frontend_bound') / 4))
print('  Latency:                         %15u' % (td.get('frontend_latency') / 4))
print('  Bandwidth:                       %15u' % (td.get('frontend_bandwidth') / 4))
print('Backend Bound:             %15u' % (td.get('l1_backend_bound') / 4))
print('  Memory Bound:                    %15u' % (td.get('memory_bound') / 4))
print('  Core Bound:                      %15u' % (td.get('core_bound') / 4))
print('Bad Speculation:           %15u' % (td.get('l1_bad_speculation') / 4))
print('  Branch Mispredicts:              %15u' % (td.get('branch_mispredicts') / 4))
print('  Machine Clears:                  %15u' % (td.get('machine_clears') / 4))

print('\n\nTop 15 ips')
ip_sgs = sgs_aggregate(sgs, lambda a: a['symoff'])
metric_table(ip_sgs, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED',
    'l1_backend_bound', 'l1_frontend_bound', 'l1_bad_speculation'], TopDownAbs,
        max_rows=15,
        attr_map=lambda a: hex(a))

print('\n\nTop 15backend bound ips')
metric_table(ip_sgs, 'l1_backend_bound', ['l1_backend_bound'], TopDownAbs, 
        max_rows=15,
        attr_map=lambda a: hex(a))


