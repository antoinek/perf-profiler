from profiling import *

def metric_table(sgs, primary, cols, analysis, cutoff_frac=None,
        attr_map=lambda x:x, attr_width=50, col_format='%15u',
        max_rows=None):
    total_primary = 0
    for sg in sgs:
        x = analysis(sg).get(primary)
        if x is not None:
            total_primary += x

    aggfr = 0
    sorted_sgs = sorted(sgs, key=lambda x: analysis(x).get(primary), reverse=True)
    n = 0
    for sg in sorted_sgs:
        an = analysis(sg)
        fr = float(an.get(primary)) / total_primary * 100

        s = attr_map(sg.attrs).ljust(attr_width)
        s += '  %05.2f%%  ' % (fr)
        i = 0
        for c in cols:
            if isinstance(col_format, str):
                f = col_format
            else:
                f = col_format[i]

            s += ('  ' + f) % (an.get(c))
            i += 1

        print(s)

        aggfr += fr
        if cutoff_frac is not None and aggfr > cutoff_frac:
            break

        n = n + 1
        if max_rows is not None and n >= max_rows:
            break

def hierarchic_table(sgs, primary, cols, analysis, classifiers,cutoff=None):
    top_sg = SampleGroup()
    for sg in sgs:
        top_sg.merge_in(sg)
    hierarchic_helper(sgs, primary, cols, analysis, 'top', top_sg, classifiers,
            0, cutoff)

def hierarchic_helper(sgs, primary, cols, analysis, top_class, top_sg,
        classifiers, indent, cutoff=None):
    classifier = classifiers[0] if len(classifiers) >= 1 else None
    sg_per = {}
    for sg in sgs:
        if classifier:
            c = classifier(sg.attrs)
            if c not in sg_per:
                sg_per[c] = (SampleGroup(), [])

            sg_per[c][0].merge_in(sg)
            sg_per[c][1].append(sg)

    ind_s = '\t' * indent
    an = analysis(top_sg)
    pri = an.get(primary)
    if pri is None:
        pri = -1

    if cutoff is not None and pri < cutoff:
        return

    s = ''
    for c in cols:
        s += ('\t%d') % (an.get(c))

    print('%s%s\t%15d%s' % (ind_s, top_class, pri, s))
    if len(classifiers) >= 1:
        children = sorted(sg_per.items(), key=lambda t: analysis(t[1][0]).get(primary), reverse=True)
        for k,(sg,chi) in children:
            hierarchic_helper(chi, primary, cols, analysis, k, sg,
                    classifiers[1:], indent + 1, cutoff)

