from profiling import *
from topdown import *
from report_utils import *
from splittcp import *
import sys


(md, sgs) = load_file(sys.argv[1])

if sys.argv[2] == 'app':
    tname = 'echo-w0'
    classifier = classify_stcp_app
elif sys.argv[2] == 'linux':
    tname = 'echo-w0'
    classifier = classify_linux
else:
    tname = 'stcp-fp-0'
    classifier = classify_stcp



tid = find_tid(md, tname)
print(md['duration'])
sgs = sgs_filter(sgs, lambda a: a['tid'] == tid)
balance_counters(sgs)

sym_sgs = sgs_aggregate(sgs, lambda a: (a['dso'], a['symbol']))
cat_sgs = sgs_aggregate(sgs, classifier)
total_sg = sgs_aggregate(sgs, lambda a: '')[0]




metric_table(sym_sgs, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED',
    'CYCLE_ACTIVITY:STALLS_MEM_ANY', 'cpi'], TopDown, cutoff_frac=99,
    col_format=['%15u', '%15u', '%05.2f'],
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])


print('\n\n\n')

td = TopDown(total_sg)

print('IPC:                       %5.2f' % (td.get('ipc')))
print('Retiring:                  %5.2f%%' % (td.get('l1_retiring') * 100))
print('  General Retirement:              %5.2f%%' % (td.get('l2_general_retirement') * 100))
print('  Microcode Sequence:              %5.2f%%' % (td.get('l2_microcode_sequencer') * 100))
print('Frontend Bound:            %5.2f%%' % (td.get('l1_frontend_bound') * 100))
print('  Latency:                         %5.2f%%' % (td.get('l2_frontend_latency') * 100))
print('  Bandwidth:                       %5.2f%%' % (td.get('l2_frontend_bandwidth') * 100))
print('Backend Bound:             %5.2f%%' % (td.get('l1_backend_bound') * 100))
print('  Memory Bound:                    %5.2f%%' % (td.get('l2_memory_bound') * 100))
print('  Core Bound:                      %5.2f%%' % (td.get('l2_core_bound') * 100))
print('Bad Speculation:           %5.2f%%' % (td.get('l1_bad_speculation') * 100))
print('  Branch Mispredicts:              %5.2f%%' % (td.get('l2_branch_mispredicts') * 100))
print('  Machine Clears:                  %5.2f%%' % (td.get('l2_machine_clears') * 100))

print('\n\n\n')

td = TopDownAbs(total_sg)

print('Retiring:                  %15u' % (td.get('l1_retiring')))
print('  General Retirement:              %15u' % (td.get('general_retirement')))
print('  Microcode Sequence:              %15u' % (td.get('microcode_sequencer')))
print('Frontend Bound:            %15u' % (td.get('l1_frontend_bound')))
print('  Latency:                         %15u' % (td.get('frontend_latency')))
print('  Bandwidth:                       %15u' % (td.get('frontend_bandwidth')))
print('Backend Bound:             %15u' % (td.get('l1_backend_bound')))
print('  Memory Bound:                    %15u' % (td.get('memory_bound')))
print('  Core Bound:                      %15u' % (td.get('core_bound')))
print('Bad Speculation:           %15u' % (td.get('l1_bad_speculation')))
print('  Branch Mispredicts:              %15u' % (td.get('branch_mispredicts')))
print('  Machine Clears:                  %15u' % (td.get('machine_clears')))


print('\n\n\n')


metric_table(cat_sgs, 'CPU_CLK_UNHALTED',
    ['l1_retiring', 'l1_frontend_bound', 'l1_backend_bound', 'l1_bad_speculation'],
    TopDown, col_format='%04.3f')

print('\n\n\n')


metric_table(sym_sgs, 'CPU_CLK_UNHALTED',
    ['l1_retiring', 'l1_frontend_bound', 'l1_backend_bound', 'l1_bad_speculation'],
    TopDown, cutoff_frac=99, col_format='%04.3f',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])


print('\n\n\n')

metric_table(sym_sgs, 'CYCLE_ACTIVITY:STALLS_MEM_ANY',
    ['CYCLE_ACTIVITY:STALLS_MEM_ANY'], TopDown, cutoff_frac=99, col_format='%15u',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])

print('\n\n\n')
metric_table(sym_sgs, 'l1_backend_bound',
    ['l1_backend_bound', 'memory_bound', 'core_bound'], TopDownAbs, cutoff_frac=99,
    col_format='%15u',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])

print('\n\n\n')

metric_table(sgs, 'l1_backend_bound',
    ['l1_backend_bound', 'memory_bound', 'core_bound', 'CPU_CLK_UNHALTED'],
    TopDownAbs, cutoff_frac=95,
    col_format='%15u',
    attr_width=50, attr_map=lambda x: x['symbol'] + '.' + hex(x['symoff']))

