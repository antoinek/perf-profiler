import json
import collections.abc


class SGAttrs(collections.abc.Hashable):
    def __init__(self, ip, pid, tid, symbol, symoff, dso, cust=None):
        self._ip = ip
        self._pid = pid
        self._tid = tid
        self._symbol = symbol
        self._symoff = symoff
        self._dso = dso
        self._cust = cust

    def mask_out(self, field):
        return self.replace(field, None)

    def subset(self, fields):
        ip = None
        pid = None
        tid = None
        symbol = None
        symoff = None
        dso = None
        cust = None

        for field in fields:
            if field == 'ip':
                ip = self._ip
            elif field == 'pid':
                pid = self._pid
            elif field == 'tid':
                tid = self._tid
            elif field == 'symbol':
                symbol = self._symbol
            elif field == 'symoff':
                symoff = self._symoff
            elif field == 'dso':
                dso = self._dso
            elif field == 'cust':
                cust = self._cust
            else:
                raise 'Bad field'

        return SGAttrs(ip, pid, tid, symbol, symoff, dso, cust)

    def replace(self, field, value):
        ip = self._ip
        pid = self._pid
        tid = self._tid
        symbol = self._symbol
        symoff = self._symoff
        dso = self._dso
        cust = self._cust

        if field == 'ip':
            ip = value
        elif field == 'pid':
            pid = value
        elif field == 'tid':
            tid = value
        elif field == 'symbol':
            symbol = value
        elif field == 'symoff':
            symoff = value
        elif field == 'dso':
            dso = value
        elif field == 'cust':
            cust = value
        else:
            raise KeyError()

        return SGAttrs(ip, pid, tid, symbol, symoff, dso, cust)

    def _tuple(self):
        return (self._ip, self._pid, self._tid, self._symbol, self._symoff, \
                self._dso, self._cust)

    def __getitem__(self, field):
        if field == 'ip':
            return self._ip
        elif field == 'pid':
            return self._pid
        elif field == 'tid':
            return self._tid
        elif field == 'symbol':
            return self._symbol
        elif field == 'symoff':
            return self._symoff
        elif field == 'dso':
            return self._dso
        elif field == 'cust':
            return self._cust
        else:
            raise KeyError()

    def __eq__(self, other):
        return self._tuple() == other._tuple()

    def __hash__(self):
        return hash(self._tuple())



class SampleGroup(object):
    def __init__(self):
        self.events = {}
        self.attrs = {}

    def merge_in(self, other):
        for k in other.events.keys():
            if k not in self.events.keys():
                self.events[k] = {'value': 0, 'time_e': 0, 'time_r': 0}

            self.events[k]['value'] += other.events[k]['value']
            self.events[k]['time_r'] += other.events[k]['time_r']
            self.events[k]['time_e'] += other.events[k]['time_e']

    def add_event(self, ev, value, time_e, time_r):
        if ev not in self.events.keys():
            self.events[ev] = {'value': value, 'time_e': time_e, 'time_r': time_r}
        else:
            self.events[ev]['value'] += value
            self.events[ev]['time_e'] += time_e
            self.events[ev]['time_r'] += time_r

    def get(self, ev):
        if ev not in self.events.keys():
            return None
        return self.events[ev]['value']


class SGAnalysis(object):
    def __init__(self, sg):
        self.sg = sg

    def ctr(self, ev):
        return self.sg.get(ev)


    def get(self, ev):
        x = self.sg.get(ev)
        if x is not None:
            return x

        return getattr(self, 'get_' + ev)()


    def add(self, a, b):
        if a is None or b is None:
            return None
        return a + b

    def sub(self, a, b):
        if a is None or b is None:
            return None
        return a - b

    def mul(self, a, b):
        if a is None or b is None:
            return None
        return a * b

    def div(self, a, b):
        if a is None or b is None:
            return None
        elif b == 0:
            return float('inf')

        return float(a) / float(b)


# Load trace from json file
# Returns (metadata, list of sample groups)
def load_file(path):
    with open(path, 'r') as f:
        data = json.load(f)
    return load_from_object(data)

def load_exp_file(path):
    with open(path, 'r') as f:
        data = json.load(f)
    return load_from_object(data['profiler']['data'])


def load_from_object(data):
    metadata = {}
    for k in data:
        if k == 'events':
            continue
        metadata[k] = data[k]

    counters = list(data['event_descs'].keys())
    sgs = []
    for ev in data['events']:
        if len(ev.keys()) == 0:
            continue

        sg = SampleGroup()
        sg.attrs = SGAttrs(ev['ip'], ev['pid'], ev['tid'], ev['symbol'],
                ev['symoff'], ev['dso'], None)
        for cnt in counters:
            if cnt not in ev['counters'].keys():
                sg.add_event(cnt, 0, 0, 0)
            else:
                sg.add_event(cnt, ev['counters'][cnt]['value'],
                        ev['counters'][cnt]['time_enabled'],
                        ev['counters'][cnt]['time_running'])

        sgs.append(sg)

    sgs = patch_raw_sgs(sgs)
    return (metadata, sgs)

# Patches up some snafus in raw sample group list
# (currently only one miscaptured symbol in glibc)
def patch_raw_sgs(sgs):
    for sg in sgs:
        if sg.attrs['symbol'] == '__nss_group_lookup':
            # not sure why the glibc symbol for this is off
            sg.attrs = sg.attrs.replace('symbol', 'memcpy')
        elif sg.attrs['ip'] >= 0xfffffe0000000000 and sg.attrs['ip'] < 0xffffff0000000000:
            # this is missing in the symbol table for current kernels, but will
            # be there in later ones (maybe 4.16+?)
            sg.attrs = sg.attrs.replace('symbol',
                    '__entry_SYSCALL_64_trampoline').replace('dso', 'vmlinux')
    return sgs

# Normalize all counters to same time enabled
# (modifies in-place)
def balance_counters(sgs):
    times_r = {}
    times_e = {}
    for sg in sgs:
        for k in sg.events.keys():
            if k not in times_r.keys():
                times_r[k] = 0
                times_e[k] = 0
            times_r[k] += sg.events[k]['time_r']
            times_e[k] += sg.events[k]['time_e']

    fracs = {}
    for k in times_r.keys():
        if times_r[k] != 0:
            fracs[k] = float(times_e[k]) / float(times_r[k])
        else:
            fracs[k] = 0

    for sg in sgs:
        for k in sg.events.keys():
            sg.events[k]['value'] = int(sg.events[k]['value'] * fracs[k])
            sg.events[k]['time_r'] = int(sg.events[k]['time_r'] * fracs[k])

def scale_counters(sgs, fac):
    for sg in sgs:
        for k in sg.events:
            sg.events[k]['value'] *= fac
            sg.events[k]['time_r'] *= fac
            sg.events[k]['time_e'] *= fac

def find_tid(md, tname):
    for p in md['procs'].values():
        for i in p['threads'].keys():
            if p['threads'][i]['comm'] == tname:
                return int(i)
    return None

def find_tids_prefix(md, tname):
    tids = []
    for p in md['procs'].values():
        for i in p['threads'].keys():
            if p['threads'][i]['comm'].startswith(tname):
                tids.append(int(i))
    return tids

def find_pid(md, pname):
    for pid in md['procs'].keys():
        if md['procs'][pid]['comm'] == pname:
            return int(pid)
    return None

def find_tids_proc(md, pname):
    tids = []
    pid = find_pid(md, pname)
    for p in md['procs'].values():
        if p['comm'] != pname:
            continue

        for i in p['threads'].keys():
            tids.append(int(i))
    return tids

def sgs_filter(sgs, pred):
    new_sgs = []

    for sg in sgs:
        if pred(sg.attrs):
            new_sgs.append(sg)
    return new_sgs

def sgs_aggregate(sgs, mapper):
    new_sgs = {}

    for sg in sgs:
        k = mapper(sg.attrs)
        if k not in new_sgs:
            nsg = SampleGroup()
            nsg.attrs = k
            new_sgs[k] = nsg
        else:
            nsg = new_sgs[k]

        nsg.merge_in(sg)

    return list(new_sgs.values())

def calculate_icache_footprint(sgs, primary='CPU_CLK_UNHALTED',
        cutoff_frac=.99):
    total_primary = 0
    for sg in sgs:
        x = sg.get(primary)
        if x is not None:
            total_primary += x

    ips = set()
    agg = 0
    sorted_sgs = sorted(sgs, key=lambda x: x.get(primary), reverse=True)
    for sg in sorted_sgs:
        ips.add(sg.attrs['ip'] & ~63)

        agg += sg.get(primary)
        if float(agg) / total_primary > cutoff_frac:
            break

    return len(ips)
