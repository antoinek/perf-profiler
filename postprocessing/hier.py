from profiling import *
from topdown import *
from report_utils import *
from splittcp import *
import sys




def linux_component(a):
    if a['dso'] == 'vmlinux':
        return 'kernel'
    else:
        return 'app'

def linux_subcomponent(a):
    if a['dso'] == 'vmlinux':
        return classify_linux_kernel(a)
    elif a['dso'].startswith('libc') or a['dso'].startswith('libpthread'):
        return 'libc'
    elif a['dso'].startswith('echo') or a['dso'].startswith('flexkvs'):
        return 'app'
    else:
        return 'other'

def ix_component(a):
    if a['dso'] == 'ix':
        return 'ix'
    elif a['dso'] == 'echoserver':
        return 'app'
    else:
        return 'other'

def ix_subcomponent(a):
    if a['dso'] == 'ix':
        return classify_ix_symbol(a)
    elif a['dso'] == 'echoserver':
        return 'app'
    else:
        return 'other'

def tas_component(a):
    return classify_stcp(a)

def tas_app_component(a):
    return classify_stcp_app(a)

def class_symbol(a):
    return a['symbol']

def class_symoff(a):
    return hex(a['symoff'])




#(md, sgs) = load_exp_file(sys.argv[1])
(md, sgs) = load_file(sys.argv[1])

if sys.argv[2] == 'linux':
    tids = find_tids_prefix(md, 'echo-w')
    classifiers = [linux_component, linux_subcomponent]
elif sys.argv[2] == 'ix':
    tids = find_tids_prefix(md, 'ix')
    classifiers = [ix_component, ix_subcomponent]
elif sys.argv[2] == 'tas-fp':
    tids = find_tids_prefix(md, 'stcp-fp')
    classifiers = [tas_component]
elif sys.argv[2] == 'tas-app':
    tids = find_tids_prefix(md, 'flexkvs')
    classifiers = [tas_app_component]
elif sys.argv[2] == 'tas':
    #tids_app = find_tids_prefix(md, 'flexkvs-w')
    tids_app = find_tids_prefix(md, 'flexkvs-ll')
    tids_fp = find_tids_prefix(md, 'stcp')
    tids = tids_app + tids_fp
    disc_app_fp = lambda a: 'tas' if a['dso'] == 'splittcp' else 'app'
    disc_comp = lambda a: tas_component(a) if disc_app_fp(a) == 'tas' else tas_app_component(a)
    classifiers = [disc_app_fp, disc_comp]

#classifiers += [class_symbol, class_symoff]
classifiers += [class_symbol]


sgs = sgs_filter(sgs, lambda a: a['tid'] in tids)
sgs = sgs_aggregate(sgs, lambda a: a.mask_out('tid'))

# estimate 'real' values for counters
balance_counters(sgs)

# scale to per-request
scale_counters(sgs, 1 / (float(md['duration'] * md['throughput'])))


cols = ['l1_retiring', 'l1_frontend_bound', 'l1_backend_bound',
        'l1_bad_speculation']
#cols = []
hierarchic_table(sgs, 'CPU_CLK_UNHALTED', cols, TopDownAbs, classifiers, cutoff=1)
#hierarchic_table(sgs, 'INST_RETIRED:ANY_P', TopDownAbs, classifiers, cutoff=1)
#hierarchic_table(sgs, 'l1_backend_bound', TopDownAbs, classifiers, cutoff=1)
