def classify_stcp(attrs):
    sym = attrs['symbol']
    m = {
        'tx_phase1': set([
            'poll_queues',
            'fast_appctx_poll_pf',
            'fast_appctx_poll_fetch',
            'fast_appctx_poll_bump',
            'qman_set',
            'utils_rng_gen32',
            'fast_flows_bump',
            'poll_qman_fwd.isra.6',
            ]),

        'tx_phase2': set([
            'qman_poll',
            'poll_qman',
            'fast_flows_qman',
            'fast_flows_qman_pfbufs',
            'fast_flows_qman_pf',
            'flow_tx_read',

            ]),


        'rx': set([
            'fast_flows_packet',
            'fast_actx_rxq_probe',
            'fast_flows_packet_fss',
            'poll_rx',
            'fast_flows_packet_parse',
            'arx_cache_flush',
            'fast_actx_rxq_alloc',
            ]),

        'common': set([
            'dataplane_loop',
            'bufcache_prealloc',
            'qman_timestamp',
            'rte_memcpy_generic',
            'common_ring_mc_dequeue',
            'common_ring_mp_enqueue',
            ]),

        'driver': set([
            'i40e_recv_pkts_vec',
            'i40e_xmit_pkts',
            ]),


        'kernel': set([
            'poll_kernel',
            'fast_kernel_poll',
            ]),

        'slowpath': set([
            'nicif_poll',
            'nicif_connection_stats',
            'cc_poll',
            'nicif_connection_setrate',
            'appif_ctx_poll',
            ]),
    }

    for c in m.keys():
        if sym in m[c]:
            return c

    return 'other'


def classify_stcp_alt(attrs):
    sym = attrs['symbol']
    m = {
        'tx': set([
            'poll_queues',
            'fast_actx_rxq_probe',
            'fast_appctx_poll_pf',
            'fast_appctx_poll_fetch',
            'fast_appctx_poll_bump']),
        'rx': set([
            'fast_flows_packet',
            'i40e_recv_pkts_vec',
            'fast_flows_packet_fss',
            'poll_rx',
            'fast_flows_packet_parse',
            'arx_cache_flush',
            'fast_actx_rxq_alloc']),
        'qman': set([
            'fast_flows_bump',
            'qman_poll',
            'qman_set',
            'fast_flows_qman',
            'i40e_xmit_pkts',
            'flow_tx_read',
            'poll_qman',
            'fast_flows_qman_pfbufs',
            'poll_qman_fwd.isra.6',
            'utils_rng_gen32',
            'fast_flows_qman_pf']),
        'common': set([
            'dataplane_loop',
            'bufcache_prealloc',
            'qman_timestamp',
            'rte_memcpy_generic',
            'common_ring_mc_dequeue',
            'common_ring_mp_enqueue',]),
        'kernel': set([
            'poll_kernel',
            'fast_kernel_poll']),
    }

    for c in m.keys():
        if sym in m[c]:
            return c

    return 'other'



def classify_stcp_app(attrs):
    sym = attrs['symbol']
    sockets_syms = set([
            'lwip_epoll_wait',
            'lwip_read',
            'lwip_write',
            'flextcp_epoll_set',
            'flextcp_sockctx_poll',
            'flextcp_fd_slookup',
            'flextcp_sockctx_get',
            'flextcp_fd_release',
            'write',
            'read',
            '__tls_get_addr',
            'flextcp_epoll_clear',
            'flextcp_sockctx_poll_n',
            'epoll_wait',
            'flextcp_fd_elookup',
            '.plt',
            ])
    stack_syms = set([
            'flextcp_context_poll',
            'flextcp_context_tx_alloc',
            'flextcp_context_tx_done',
            'flextcp_connection_tx_send',
            'flextcp_connection_rx_done',
            'flextcp_connection_tx_alloc2',
            'flextcp_conn_txbuf_available',
            'flextcp_connection_tx_possible',
            'fastpath_poll_vec',
            'util_timeout_time_us',
            'conns_bump',
            'txq_probe',
            'kernel_poll',
            ])
    app_syms = set([
            'thread_run',
            ])

    if sym in sockets_syms:
        return 'sockets'
    elif sym in stack_syms:
        return 'stack'
    elif sym in app_syms:
        return 'app'
    elif sym == 'memcpy':
        return 'copies'
    else:
        return 'other'

def classify_linux(attrs):
    if attrs['dso'] == 'vmlinux':
        return 'k-' + classify_linux_kernel(attrs)
    elif attrs['dso'] == 'echoserver_linux':
        return 'app'
    elif attrs['dso'].startswith('libc'):
        return 'libc'
    elif attrs['dso'].startswith('libpthread'):
        return 'libc'
    else:
        return 'other'

def classify_ix(attrs):
    if attrs['dso'] == 'echoserver':
        return 'app'
    elif attrs['dso'] == 'ix':
        return 'ix'
    elif attrs['dso'].startswith('libc'):
        return 'libc'
    elif attrs['dso'].startswith('libpthread'):
        return 'libc'
    else:
        return 'other'

def classify_linux_kernel(attrs):
    sym = attrs['symbol']

    m = {
        'tcp_tx': set([
            'tcp_clean_rtx_queue',
            'tcp_transmit_skb',
            'tcp_sendmsg',
            'tcp_write_xmit',
            'tcp_schedule_loss_probe',
            'tcp_wfree',
            'tcp_event_new_data_sent',
            'tcp_established_options',
            '__tcp_select_window',
            'skb_entail',
            '__tcp_v4_send_check',
            'tcp_push',
            'tcp_send_delayed_ack',
            'tcp_send_mss',
            'tcp_options_write',
            'tcp_current_mss',
            '__tcp_push_pending_frames',
            'tcp_v4_send_check',
            'tcp_nagle_check',
            'tcp_init_tso_segs',
            'tcp_send_ack',
            'tcp_delack_timer_handler',
            'tcp_delack_timer',

            'tcp_sendmsg_locked',
            'tcp_tso_segs',
            'tcp_rate_check_app_limited',
            'tcp_rate_skb_sent',
            'tcp_small_queue_check.isra.28',
        ]),

        'tcp_rx': set([
            'tcp_ack',
            'tcp_v4_rcv',
            'tcp_recvmsg',
            'tcp_rcv_established',
            'tcp_poll',
            'tcp_gro_receive',
            'tcp_rearm_rto',
            'tcp_event_data_recv',
            'tcp_cleanup_rbuf',
            'tcp_queue_rcv',
            'tcp_update_pacing_rate',
            'tcp4_gro_receive',
            'dctcp_update_alpha',
            'dctcp_cwnd_event',
            'tcp_v4_early_demux',
            'tcp_v4_inbound_md5_hash',
            'tcp_rcv_space_adjust',
            'tcp_check_space',
            '__tcp_ecn_check_ce',
            'tcp_md5_do_lookup',
            'tcp_stream_memory_free',
            'tcp_filter',
            'tcp_v4_do_rcv',
            'tcp_prequeue',
            'tcp_release_cb',
            'tcp_parse_md5sig_option',
            'tcp_reno_cong_avoid',
            'tcp_rack_advance',
            'tcp_v4_md5_lookup',
            '__tcp_ack_snd_check',
            'tcp_parse_aligned_timestamp.part.41',
            'bictcp_cwnd_event',
            'bictcp_cong_avoid',
            'tcp_data_queue',
            'tcp_ack_update_rtt.isra.33',
            'bictcp_acked',
            'tcp_rate_gen',
            'tcp_v4_fill_cb',
            'tcp_rate_skb_delivered',
        ]),

        'ip': set([
            'ip_rcv',
            'ip_finish_output',
            'ip_finish_output2',
            'inet_gro_receive',
            'ip_queue_xmit',
            '__inet_lookup_established',
            'inet_ehashfn',
            'inet_recvmsg',
            'inet_sendmsg',
            '__ip_local_out',
            'ip_rcv_finish',
            'ipv4_mtu',
            'ip_local_deliver_finish',
            'ip_output',
            'ip_local_out',
            'ip_local_deliver',
            'ipv4_dst_check',
            'ip_send_check',
            'ip_copy_addrs',
            'raw_local_deliver',

            #questionably IP
            'packet_rcv',
            '__netif_receive_skb_core',
            'netif_skb_features',
            'netif_receive_skb_internal',
            'validate_xmit_skb.isra.103.part.104',
            'netdev_pick_tx',
            '__netdev_pick_tx',
            'dev_gro_receive',
            'sch_direct_xmit',
            'eth_type_trans',
            'skb_network_protocol',
        ]),

        'timers': set([
            'mod_timer',
            'internal_add_timer',
            '__internal_add_timer',
            'lock_timer_base.isra.34',
            'ktime_get_with_offset',
            'sk_reset_timer',
            'detach_if_pending',
            'get_nohz_timer_target',
            'read_tsc',
            'native_sched_clock',
            '__usecs_to_jiffies',
            'tcp_chrono_start',
            'tcp_rearm_rto.part.61',
            'sched_clock',
        ]),

        'bufmgt': set([
            '__slab_free',
            'skb_release_data',
            '__skb_clone',
            'kmem_cache_alloc',
            'kmem_cache_free',
            '__alloc_skb',
            'skb_release_head_state',
            'kmem_cache_alloc_node',
            '__kmalloc_node_track_caller',
            '__copy_skb_header',
            '___slab_alloc',
            '__build_skb',
            'fput',
            'consume_skb',
            'cmpxchg_double_slab.isra.55',
            '__cmpxchg_double_slab.isra.47',
            'ksize',
            '__free_page_frag',
            'kfree_skbmem',
            'kmalloc_slab',
            'skb_release_all',
            'skb_clone',
            'skb_copy_datagram_iter',
            'kfree',
            'sk_stream_alloc_skb',
            '__slab_alloc',
            '__kfree_skb',
            'swiotlb_map_page',
            'skb_push',
            'sk_free',
            'put_cpu_partial',
            'skb_put',
            'iommu_should_identity_map',
            'get_page_from_freelist',
            'intel_map_page',
            'intel_mapping_error',
            'free_one_page',
            'iommu_no_mapping',
            'dma_get_required_mask',
            '__check_heap_object',
            'prefetch_freepointer',
            'device_has_rmrr',
            'device_is_rmrr_locked',
            'page_frag_free',
            'intel_unmap',
            '__free_pages_ok',
            '__intel_map_single',
            '__alloc_pages_nodemask',
            'build_skb',
            'intel_unmap_page',
            '__kmalloc_reserve.isra.43',
        ]),

        'sync': set([
            '_raw_spin_lock',
            '_raw_spin_lock_bh',
            '_raw_spin_unlock_irqrestore',
            '_raw_spin_lock_irqsave',
            'lock_sock_nested',
            'release_sock',
            '__local_bh_enable_ip',
            '__wake_up_common',
            'rcu_irq_exit',
            'wake_up_nohz_cpu',
            'cmpxchg_double_slab.isra.61',
            '__cmpxchg_double_slab.isra.51',
            'lock_timer_base',
            '__wake_up_common_lock',
            'rcu_all_qs',
            'native_queued_spin_lock_slowpath',
            '_cond_resched',
        ]),

        'sockets': set([
            'fsnotify',
            'aa_label_sk_perm',
            '__fsnotify_parent',
            'ep_send_events_proc',
            '__fget',
            'copy_user_generic_string',
            'aa_file_perm',
            'aa_sk_perm',
            'aa_sock_msg_perm',
            'common_file_perm',
            'sock_read_iter',
            'rw_verify_area',
            'new_sync_write',
            'new_sync_read',
            'security_file_permission',
            'sk_filter_trim_cap',
            '__fget_light',
            'sock_write_iter',
            'vfs_read',
            'copy_from_iter',
            'copy_to_iter',
            'sock_def_readable',
            'sock_poll',
            'sock_put',
            'vfs_write',
            'sock_rfree',
            'ep_poll_callback',
            '__sk_dst_check',
            'SyS_write',
            '__vfs_write',
            'security_socket_recvmsg',
            'apparmor_file_permission',
            'iov_iter_init',
            'security_socket_sendmsg',
            'security_socket_sendmsg',
            '__bpf_prog_run',
            '__cgroup_bpf_run_filter_skb',
            'entry_SYSCALL_64',
            'entry_SYSCALL_64_after_swapgs',
            'entry_SYSCALL_64_fastpath',
            'sock_sendmsg',
            'SyS_read',
            'security_sock_rcv_skb',
            '__memcpy',
            '__memmove',
            'sock_recvmsg',

            'syscall_return_via_sysret',
            'copy_user_enhanced_fast_string',
            'entry_SYSCALL_64_stage2',
            '__check_object_size',
            '_copy_from_iter_full',
            'do_syscall_64',
            'ep_item_poll.isra.10',
            'entry_SYSCALL_64_after_hwframe',
            '__indirect_thunk_start',
            '__virt_addr_valid',
            'sys_read',
            'sys_write',
            'apparmor_socket_sendmsg',
            '_copy_to_iter',
            'apparmor_socket_sock_rcv_skb',
            'iov_iter_advance',
            'copyout',
            'copyin',
            'apparmor_socket_recvmsg',
            '__entry_SYSCALL_64_trampoline',
            'ep_scan_ready_list.constprop.17',
            '__fdget_pos',
            'check_stack_object'
        ]),

        'driver_tx': set([
            'ixgbe_xmit_frame_ring',
            'ixgbe_select_queue',
            'ixgbe_tx_ctxtdesc',
            'ixgbe_xmit_frame',
            'ixgbe_features_check',
            'ixgbe_fdir_add_signature_filter_82599',
            'validate_xmit_skb_list',
            'validate_xmit_skb',

            'dev_hard_start_xmit',
            '__dev_queue_xmit',
            'napi_consume_skb',
            'napi_consume_skb',

            'i40e_xmit_frame_ring',
            'i40e_lan_xmit_frame',
            'i40e_features_check',
        ]),

        'driver_rx': set([
            'ixgbe_clean_rx_irq',
            'ixgbe_poll',
            'napi_gro_receive',
            'ixgbe_alloc_rx_buffers',
            'net_rx_action',
            'ixgbe_update_itr.isra.66',
            'irq_entries_start',
            'do_IRQ',
            'handle_irq_event_percpu',
            'handle_edge_irq',
            '__do_softirq',
            '__netif_receive_skb',

            'i40e_clean_rx_irq',
            'i40e_napi_poll',
            'i40e_alloc_rx_buffers',
            'i40e_msix_clean_rings',
        ]),
    }

    for c in m.keys():
        if sym in m[c]:
            return c

    return 'other'


def classify_ix_symbol(attrs):
    sym = attrs['symbol']

    m = {
        'tcp_tx': set([
            'tcp_output',
            'tcp_write',
            'tcp_output_packet',
        ]),

        'tcp_rx': set([
            'tcp_input',
            'tcp_receive',
            'tcp_recved',
            'lwip_tcp_event',
            'tcp_parseopt',
            'tcp_update_rcv_ann_wnd',
            'tcp_input_tmp',
        ]),

        'ip': set([
            'eth_process_recv',
            'eth_input',
            'ip_send_one',
            'arp_lookup_mac',
            'eth_recv_handle_fg_transition',
            'inet_chksum_pseudo',
            'eth_process_reclaim',
            'eth_process_send',
            'eth_process_poll',
        ]),

        'timers': set([
            'timer_now',
            'timer_add_abs',
            'timer_run',
            'tcp_unified_timer_handler',
            'tcpip_tcp_timer',
            'timer_add',
        ]),

        'bufmgt': set([
            'pbuf_free.part.2',
            'pbuf_alloc',
            'pbuf_clen',
            'pbuf_header',
            'tcp_seg_free',
            'mbuf_default_done',
            'pbuf_cat',
            'mem_free',
            'pbuf_split_64k',
            'pbuf_free',
        ]),

        'interface': set([
            'bsys_dispatch.part.0',
            'bsys_tcp_sendv',
            'bsys_tcp_recv_done',
            'sys_bpoll',
            'dune_syscall_handler',
            'syscall_handler',
            'do_syscall',
            'entry_SYSCALL_64_fastpath',
            'entry_SYSCALL_64_after_swapgs',
            'entry_SYSCALL_64',
        ]),
        'sync': set([
        ]),

        'driver_tx': set([
            'i40e_tx_xmit',
            'i40e_tx_reclaim',
        ]),

        'driver_rx': set([
            'i40e_rx_poll',
        ]),
    }

    for c in m.keys():
        if sym in m[c]:
            return c

    return 'other'

