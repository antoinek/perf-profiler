from profiling import *
from topdown import *
from report_utils import *
from splittcp import *
import sys




#(md, sgs) = load_exp_file(sys.argv[1])
(md, sgs) = load_file(sys.argv[1])

if sys.argv[2] == 'linux':
    tids = find_tids_prefix(md, 'echo-w')
    classifier = classify_linux
elif sys.argv[2] == 'ix':
    tids = find_tids_prefix(md, 'ix')
    classifier = classify_linux
elif sys.argv[2] == 'stcp-fp':
    tids = find_tids_prefix(md, 'stcp-fp-')
    classifier = classify_stcp
elif sys.argv[2] == 'stcp-app':
    tids = find_tids_prefix(md, 'echo-w')
    classifier = classify_stcp_app
elif sys.argv[2] == 'stcp':
    tids_app = find_tids_prefix(md, 'echo-w')
    tids_stcp = find_tids_prefix(md, 'stcp-fp-')
    tids = tids_app + tids_stcp

    stcp_pid = find_pid(md, 'splittcp')
    classifier = lambda a: 'stcp-' + classify_stcp(a) \
            if a['pid'] == stcp_pid else \
            'app-' + classify_stcp_app(a)
elif sys.argv[2] == 'ix':
    tids = find_tids_prefix(md, 'ix')
    classifier = classify_ix


sgs = sgs_filter(sgs, lambda a: a['tid'] in tids)
sgs = sgs_aggregate(sgs, lambda a: a.mask_out('tid'))

# estimate 'real' values for counters
balance_counters(sgs)

# scale to per-request
scale_counters(sgs, 1 / (float(md['duration'] * md['throughput'])))


print('Icache footprint:', calculate_icache_footprint(sgs, cutoff_frac=1) * 64 / 1024, 'KB')

total_sg = sgs_aggregate(sgs, lambda a: '')[0]
print('Cycles per request: %d' % (total_sg.get('CPU_CLK_UNHALTED')))
print('Instructions per request: %d' % (total_sg.get('INST_RETIRED:ANY_P')))
print('\n')


td = TopDown(total_sg)

print('CPI:                       %5.2f' % (td.get('cpi')))
print('Retiring:                  %5.2f%%' % (td.get('l1_retiring') * 100))
print('  General Retirement:              %5.2f%%' % (td.get('l2_general_retirement') * 100))
print('  Microcode Sequence:              %5.2f%%' % (td.get('l2_microcode_sequencer') * 100))
print('Frontend Bound:            %5.2f%%' % (td.get('l1_frontend_bound') * 100))
print('  Latency:                         %5.2f%%' % (td.get('l2_frontend_latency') * 100))
print('  Bandwidth:                       %5.2f%%' % (td.get('l2_frontend_bandwidth') * 100))
print('Backend Bound:             %5.2f%%' % (td.get('l1_backend_bound') * 100))
print('  Memory Bound:                    %5.2f%%' % (td.get('l2_memory_bound') * 100))
print('  Core Bound:                      %5.2f%%' % (td.get('l2_core_bound') * 100))
print('Bad Speculation:           %5.2f%%' % (td.get('l1_bad_speculation') * 100))
print('  Branch Mispredicts:              %5.2f%%' % (td.get('l2_branch_mispredicts') * 100))
print('  Machine Clears:                  %5.2f%%' % (td.get('l2_machine_clears') * 100))

print('\n\n\n')


td = TopDownAbs(total_sg)

print('Retiring:                  %15u' % (td.get('l1_retiring') / 4))
print('  General Retirement:              %15u' % (td.get('general_retirement') / 4))
print('  Microcode Sequence:              %15u' % (td.get('microcode_sequencer') / 4))
print('Frontend Bound:            %15u' % (td.get('l1_frontend_bound') / 4))
print('  Latency:                         %15u' % (td.get('frontend_latency') / 4))
print('  Bandwidth:                       %15u' % (td.get('frontend_bandwidth') / 4))
print('Backend Bound:             %15u' % (td.get('l1_backend_bound') / 4))
print('  Memory Bound:                    %15u' % (td.get('memory_bound') / 4))
print('  Core Bound:                      %15u' % (td.get('core_bound') / 4))
print('Bad Speculation:           %15u' % (td.get('l1_bad_speculation') / 4))
print('  Branch Mispredicts:              %15u' % (td.get('branch_mispredicts') / 4))
print('  Machine Clears:                  %15u' % (td.get('machine_clears') / 4))


print('\n\n\n')

print('Breakdown to categories (cycles, cpi)')
cat_sgs = sgs_aggregate(sgs, classifier)
metric_table(cat_sgs, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED', 'cpi'], TopDown,
        col_format=['%5d', '%5.2f'])


print('\n\n\n')

print('Top 15 symbols')
sym_sgs = sgs_aggregate(sgs, lambda a: (a['dso'], a['symbol']))
metric_table(sym_sgs, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED'], TopDown,
        max_rows=15,
        attr_map=lambda a: a[0] + ':' + a[1])

#sys.exit(0)
print('\n\n\n')
print('Top 10 symbols Backend bound')
sym_sgs = sgs_aggregate(sgs, lambda a: (a['dso'], a['symbol']))
metric_table(sym_sgs, 'l1_backend_bound', ['l1_backend_bound'], TopDownAbs,
        max_rows=10,
        attr_map=lambda a: a[0] + ':' + a[1])

print('\n\n\n')
print('Top 10 symbols Frontend bound')
sym_sgs = sgs_aggregate(sgs, lambda a: (a['dso'], a['symbol']))
metric_table(sym_sgs, 'l1_frontend_bound', ['l1_frontend_bound'], TopDownAbs,
        max_rows=10,
        attr_map=lambda a: a[0] + ':' + a[1])

print('\n\n\n')
print('Top 10 symbols Bad Speculation')
sym_sgs = sgs_aggregate(sgs, lambda a: (a['dso'], a['symbol']))
metric_table(sym_sgs, 'l1_bad_speculation', ['l1_bad_speculation'], TopDownAbs,
        max_rows=10,
        attr_map=lambda a: a[0] + ':' + a[1])









sgs_other = sgs_aggregate(
        sgs_filter(sgs, lambda a: classify_linux(a) == 'k-other'),
        lambda a: a['symbol'])

metric_table(sgs_other, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED'], TopDown,
        col_format='%04.3f', cutoff_frac=100)

print('\n\n\n')

sgs_dns = sgs_aggregate(
        sgs_filter(sgs, lambda a: a['symbol'] == 'exit_dns_resolver'),
        lambda a: hex(a['ip']))
metric_table(sgs_dns, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED'], TopDown)

sys.exit(0)

metric_table(sym_sgs, 'CPU_CLK_UNHALTED', ['CPU_CLK_UNHALTED',
    'CYCLE_ACTIVITY:STALLS_MEM_ANY', 'cpi'], TopDown, cutoff_frac=99,
    col_format=['%15u', '%15u', '%05.2f'],
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])


print('\n\n\n')



metric_table(sym_sgs, 'CPU_CLK_UNHALTED',
    ['l1_retiring', 'l1_frontend_bound', 'l1_backend_bound', 'l1_bad_speculation'],
    TopDown, cutoff_frac=99, col_format='%04.3f',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])


print('\n\n\n')

metric_table(sym_sgs, 'CYCLE_ACTIVITY:STALLS_MEM_ANY',
    ['CYCLE_ACTIVITY:STALLS_MEM_ANY'], TopDown, cutoff_frac=99, col_format='%15u',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])

print('\n\n\n')
metric_table(sym_sgs, 'l1_backend_bound',
    ['l1_backend_bound', 'memory_bound', 'core_bound'], TopDownAbs, cutoff_frac=99,
    col_format='%15u',
    attr_width=50, attr_map=lambda x: x[0] + '.' + x[1])

print('\n\n\n')

metric_table(sgs, 'l1_backend_bound',
    ['l1_backend_bound', 'memory_bound', 'core_bound', 'CPU_CLK_UNHALTED'],
    TopDownAbs, cutoff_frac=95,
    col_format='%15u',
    attr_width=50, attr_map=lambda x: x['symbol'] + '.' + hex(x['symoff']))

