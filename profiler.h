#ifndef PROFILER_H_
#define PROFILER_H_

#include <stdint.h>
#include <stddef.h>
#include <sys/types.h>

struct config_counter {
  char *counter;
  uint64_t period;
};

struct config_process {
  pid_t pid;
  char *comm;
  struct config_thread *threads;
  size_t num_threads;

  struct config_process *next;
};

struct config_thread {
  pid_t tid;
  char *comm;
  struct config_process *proc;

  struct config_thread *next;
  struct config_thread *next_proc;
};

extern struct config_counter *config_counters;
extern size_t config_counter_num;

extern struct config_process *config_procs;
extern size_t config_procs_num;

extern struct config_thread *config_threads;
extern size_t config_threads_num;

extern uint64_t config_duration;

int config_init(int argc, char *argv[]);

void profile_sample(pid_t pid, pid_t tid, uint16_t cpu, uint64_t ip,
    uint64_t value, uint64_t time_enabled, uint64_t time_running,
    uint32_t event);

int events_init(void);
int events_enable(void);
unsigned events_poll(void);

int symbols_init(void);
int symbols_lookup(pid_t pid, uint64_t ip, char **sym, char **dso,
    uint64_t *off);

#endif
