#include <err.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>

#include <perfmon/pfmlib_perf_event.h>

#include "profiler.h"

#define HT_SIZE (4096)

struct target {
  uint64_t ip;
  uint32_t pid;
  uint32_t tid;

  struct target *next;
  struct {
    uint64_t value;
    uint64_t time_enabled;
    uint64_t time_running;
  } counters[];
};

struct target *target_table[HT_SIZE];
static volatile int exited = 0;

static inline uint32_t hash64(uint64_t key)
{
  key = (~key) + (key << 18); // key = (key << 18) - key - 1;
  key = key ^ (key >> 31);
  key = key * 21; // key = (key + (key << 2)) + (key << 4);
  key = key ^ (key >> 11);
  key = key + (key << 6);
  key = key ^ (key >> 22);
  return key;
}

static inline uint32_t hash_target(uint64_t ip, uint32_t pid, uint32_t tid)
{
  uint32_t h;

  h = hash64(ip);
  h = hash64(((uint64_t) h << 32) | pid);
  h = hash64(((uint64_t) h << 32) | tid);

  return h;
}

static inline struct target *target_lookup(uint64_t ip, uint32_t pid,
    uint32_t tid)
{
  uint32_t h = hash_target(ip, pid, tid) % HT_SIZE;
  struct target *t = target_table[h];

  while (t != NULL && (t->ip != ip || t->pid != pid || t->tid != tid)) {
    t = t->next;
  }

  /* found an entry */
  if (t != NULL)
    return t;

  /* need to allocate new entry */
  t = calloc(1, sizeof(*t) + sizeof(t->counters[0]) * config_counter_num);
  if (t == NULL) {
    perror("target_lookup: calloc failed");
    abort();
  }

  t->ip = ip;
  t->pid = pid;
  t->tid = tid;

  t->next = target_table[h];
  target_table[h] = t;

  return t;
}

void profile_sample(pid_t pid, pid_t tid, uint16_t cpu, uint64_t ip,
    uint64_t value, uint64_t time_enabled, uint64_t time_running,
    uint32_t event)
{
  struct target *t = target_lookup(ip, pid, tid);

  t->counters[event].value += value;
  t->counters[event].time_enabled += time_enabled;
  t->counters[event].time_running += time_running;
}

static void target_dump(struct target *t)
{
  size_t i;
  uint64_t symoff;
  char *symbol, *dso;

  if (symbols_lookup(t->pid, t->ip, &symbol, &dso, &symoff) != 0) {
    symbol = "";
    dso = "";
    symoff = 0;
  }

  printf(
      "  {\n"
      "     \"ip\": %lu,\n"
      "     \"pid\": %u,\n"
      "     \"tid\": %u,\n"
      "     \"symbol\": \"%s\",\n"
      "     \"symoff\": %lu,\n"
      "     \"dso\": \"%s\",\n"
      "     \"counters\": {\n",
      t->ip, t->pid, t->tid, symbol, symoff, dso);

  for (i = 0; i < config_counter_num; i++) {
    if (t->counters[i].value == 0)
      continue;
    printf(
        "       \"%s\": { \"value\": %lu, \"time_enabled\": %lu, "
        "\"time_running\": %lu },\n",
        config_counters[i].counter,
        t->counters[i].value, t->counters[i].time_enabled,
        t->counters[i].time_running);

  }

  printf("       \"\": { }\n");
  printf(
      "     }\n"
      "  },\n");
}

static void profile_dump(double dur)
{
  struct target *t;
  struct config_process *p;
  struct config_thread *th;
  size_t i, j;

  printf("{\n");

  /* dump out duration */
  printf("\"duration\": %lf,\n", dur);

  /* dump out processes */
  printf("\"procs\": {\n");
  for (i = 0, p = config_procs; p != NULL; i++, p = p->next) {
    printf("  \"%u\": {\n", p->pid);
    printf("    \"comm\": \"%s\",\n", p->comm);

    /* dump out threads */
    printf("    \"threads\": {\n");
    for (j = 0, th = p->threads; th != NULL; j++, th = th->next_proc) {
      printf("      \"%u\": {\n", th->tid);
      printf("        \"comm\": \"%s\"\n", th->comm);
      if (j != p->num_threads - 1)
        printf("      },\n");
      else
        printf("      }\n");
    }
    printf("    }\n");

    if (i != config_procs_num - 1)
      printf("  },\n");
    else
      printf("  }\n");
  }
  printf("},\n");

  /* dump out counters */
  printf("\"event_descs\": {\n");
  for (i = 0;  i < config_counter_num; i++) {
    printf("  \"%s\": { \"period\": %lu }", config_counters[i].counter,
        config_counters[i].period);
    if (i != config_counter_num - 1)
      printf(",\n");
    else
      printf("\n");
  }
  printf("},\n");

  /* dump out events */
  printf("\"events\": [\n");
  for (i = 0; i < HT_SIZE; i++) {
    t = target_table[i];
    while (t != NULL) {
      target_dump(t);
      t = t->next;
    }
  }
  printf("  {}\n");
  printf("]\n");
  printf("}\n");
}

static void int_handler(int dummy)
{
  exited = 1;
}

int main(int argc, char *argv[])
{
  size_t i;
  struct timespec ts_start, ts_end;
  uint64_t td;

  signal(SIGINT, int_handler);

  if (config_init(argc, argv) != 0) {
    return -1;
  }

  if (symbols_init() != 0) {
    return -1;
  }

  clock_gettime(CLOCK_MONOTONIC, &ts_start);

  if (events_init() != 0) {
    return -1;
  }

  if (events_enable() != 0) {
    return -1;
  }


  i = 0;
  while (!exited) {
    events_poll();
    i++;
    if (i % 10000 == 0 && config_duration != 0) {
      clock_gettime(CLOCK_MONOTONIC, &ts_end);
      td = 1000000000ULL * (ts_end.tv_sec - ts_start.tv_sec) +
        (ts_end.tv_nsec - ts_start.tv_nsec);

      if (td / 1000000000ULL >= config_duration)
        exited = 1;
    }
  }

  clock_gettime(CLOCK_MONOTONIC, &ts_end);
  td = 1000000000ULL * (ts_end.tv_sec - ts_start.tv_sec) +
      (ts_end.tv_nsec - ts_start.tv_nsec);

  profile_dump(((double) td) / 1000000000ULL);

  return 0;
}
