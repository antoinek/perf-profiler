CFLAGS= -O3 -Wall -g
LDFLAGS= -g

all: pfmprof

pfmprof: pfmprof.o config.o events.o symbols.o
pfmprof: LDLIBS+=-lpfm -lbfd

clean:
	rm -f *.o pfmprof

.PHONY: all clean
