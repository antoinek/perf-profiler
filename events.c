#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>

#include <perfmon/pfmlib_perf_event.h>

#include "profiler.h"


struct tid_group {
  struct event_desc **descs;
  volatile struct perf_event_mmap_page *mp;
  pid_t tid;
};

struct event_desc {
  struct perf_event_attr attr;
  int fd;
  int my_id;
  uint64_t id;

  uint64_t last_value;
  uint64_t last_time_enabled;
  uint64_t last_time_running;
};

static struct tid_group *tid_groups;
static size_t tid_groups_num;

static struct event_desc *event_open(const char *str, pid_t pid, int output_fd,
    int my_id, uint64_t period)
{
  struct event_desc *ed;
  pfm_perf_encode_arg_t evt;
  char *fstr = NULL;
  int ret;

  if ((ed = calloc(1, sizeof(*ed))) == NULL) {
    perror("event_open: calloc failed");
    return NULL;
  }

  ed->my_id = my_id;

  ed->attr.size = sizeof(ed->attr);

  memset(&evt, 0, sizeof(evt));
  evt.size = sizeof(evt);
  evt.attr = &ed->attr;
  evt.fstr = &fstr;

  ret = pfm_get_os_event_encoding(str,
      PFM_PLM3 | PFM_PLM2 | PFM_PLM1 | PFM_PLM0 | PFM_PLMH,
      PFM_OS_PERF_EVENT, &evt);
  if (ret != PFM_SUCCESS) {
    fprintf(stderr, "event_open: cannot get encoding: %s\n", pfm_strerror(ret));
    return NULL;
  }

  ed->attr.exclude_guest = 0;
  ed->attr.sample_id_all = 1;

  ed->attr.sample_period = period;

  ed->attr.sample_type = PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_TIME |
    PERF_SAMPLE_READ | PERF_SAMPLE_ID | PERF_SAMPLE_CPU |
    PERF_SAMPLE_IDENTIFIER;

  ed->attr.read_format = PERF_FORMAT_TOTAL_TIME_ENABLED |
    PERF_FORMAT_TOTAL_TIME_RUNNING;

  ed->attr.disabled = 1;
  ed->attr.mmap = 1;
  ed->attr.comm = 1;

  ret = perf_event_open(&ed->attr, pid, -1, -1, 0);
  if (ret < 0) {
    perror("event_open: perf_event_open failed");
    free(ed);
    return NULL;
  }
  ed->fd = ret;

  ret = ioctl(ed->fd, PERF_EVENT_IOC_ID, &ed->id);
  if (ret != 0) {
    perror("event_open: ioctl event id failed");
    close(ed->fd);
    free(ed);
    return NULL;
  }

  if (output_fd >= 0) {
    ret = ioctl(ed->fd, PERF_EVENT_IOC_SET_OUTPUT, output_fd);
    if (ret != 0) {
      perror("event_open: ioctl set output id failed");
      close(ed->fd);
      free(ed);
      return NULL;
    }
  }

  ed->last_value = 0;
  ed->last_time_enabled = 0;
  ed->last_time_running = 0;

  return ed;
}

static void event_enable(struct event_desc *ed)
{
  int ret;

  ret = ioctl(ed->fd, PERF_EVENT_IOC_ENABLE, 0);
  if (ret != 0) {
    perror("event_enable: ioctl event enable failed");
  }
}

static struct event_desc *event_lookup_id(struct tid_group *tg, uint64_t id)
{
  size_t i;
  for (i = 0; i < config_counter_num; i++) {
    if (tg->descs[i]->id == id)
      return tg->descs[i];
  }

  return NULL;
}

static void *event_map(int fd)
{
  void *m;

  m = mmap(NULL, 0x1000 + 16 * 1024 * 1024, PROT_READ | PROT_WRITE,
      MAP_LOCKED | MAP_POPULATE | MAP_SHARED, fd, 0);
  if (m == MAP_FAILED) {
    perror("event_map: mmap failed");
    return NULL;
  }

  return m;
}

struct sample_event {
  struct perf_event_header hdr;
  uint64_t sample_id;
  uint64_t ip;
  uint32_t pid;
  uint32_t tid;
  uint64_t time;
  uint64_t id;
  uint32_t cpu;
  uint32_t res;

  uint64_t value;

  uint64_t time_enabled;
  uint64_t time_running;
} __attribute__((packed));

static void event_sample(struct tid_group *tg, struct perf_event_header *hdr)
{
  struct sample_event *se = (struct sample_event *) hdr;
  struct event_desc *ed;

  if (se->tid != tg->tid) {
    fprintf(stderr, "event_sample: event tid does not match (%u != %u)\n",
        se->tid, tg->tid);
    return;
  }

  if (se->hdr.size != sizeof(struct sample_event)) {
    fprintf(stderr, "event_sample: event smaller than expected\n");
    abort();
  }

  ed = event_lookup_id(tg, se->sample_id);

  profile_sample(se->pid, se->tid, se->cpu, se->ip, se->value - ed->last_value,
      se->time_enabled - ed->last_time_enabled,
      se->time_running - ed->last_time_running,
      ed->my_id);

  ed->last_value = se->value;
  ed->last_time_enabled = se->time_enabled;
  ed->last_time_running = se->time_running;
}

static inline uint64_t mp_head_read(volatile struct perf_event_mmap_page *mp)
{
  asm volatile("" ::: "memory");
  return mp->data_head;
}

static inline void mp_tail_write(volatile struct perf_event_mmap_page *mp,
    uint64_t tail)
{
  asm volatile("" ::: "memory");
  mp->data_tail = tail;
}

static unsigned tid_group_poll(struct tid_group *tg, unsigned n_max)
{
  unsigned n;
  volatile struct perf_event_mmap_page *mp = tg->mp;
  uint64_t size = mp->data_size;
  uint8_t *data = (uint8_t *) mp + mp->data_offset;
  struct perf_event_header *hdr;
  uint8_t buf [256];
  size_t off;
  uint64_t pos, head, ev_size;

  for (n = 0; n < n_max; n++) {
    head = mp_head_read(mp);
    pos = mp->data_tail;

    if (pos == head)
      break;

    off = pos % size;
    hdr = (struct perf_event_header *) (data + off);

    ev_size = hdr->size;
    if ((off + ev_size) % size < off) {
      fprintf(stderr, "tid_group_poll: event wraps\n");

      if (ev_size > sizeof(buf)) {
        fprintf(stderr, "tid_group_poll: event too large (%lu)\n", ev_size);
        abort();
      }
      memcpy(buf, hdr, size - off);
      memcpy(buf + (size - off), data, ev_size - (size - off));
      hdr = (struct perf_event_header *) buf;
    }

    if (hdr->type == PERF_RECORD_SAMPLE) {
      event_sample(tg, hdr);
    } else {
      fprintf(stderr, "hdr->type=%u  size=%u\n", hdr->type, hdr->size);
    }

    mp_tail_write(mp, pos + ev_size);
  }

  return n;
}


static int tid_group_init(pid_t pid, struct tid_group *tg)
{
  size_t i;
  int fd;

  tg->tid = pid;
  for (i = 0; i < config_counter_num; i++) {
    fd = (i == 0 ? -1 : tg->descs[0]->fd);
    tg->descs[i] = event_open(config_counters[i].counter, pid, fd, i,
        config_counters[i].period);
    if (tg->descs[i] == NULL) {
      fprintf(stderr, "events_initialize: counter '%s' failed\n", config_counters[i].counter);
      return -1;
    }

    if (i == 0) {
      tg->mp = event_map(tg->descs[i]->fd);
      if (tg->mp == NULL)
        return -1;
    }
  }

  return 0;
}

int events_init(void)
{
  size_t i;
  int ret;
  struct config_thread *t;

  ret = pfm_initialize();
  if (ret != PFM_SUCCESS)
    errx(1, "cannot initialize libpfm: %s", pfm_strerror(ret));

  tid_groups_num = config_threads_num;
  if ((tid_groups = calloc(tid_groups_num, sizeof(*tid_groups))) == NULL) {
    perror("events_init: calloc groups failed");
    return -1;
  }

  for (t = config_threads, i = 0; t != NULL; i++, t = t->next) {
    tid_groups[i].descs = calloc(config_counter_num,
        sizeof(tid_groups[i].descs[0]));
    if (tid_groups[i].descs == NULL) {
      perror("events_init: calloc failed");
      free(tid_groups);
      return -1;
    }

    if (tid_group_init(t->tid, &tid_groups[i]) != 0)
      return -1;
  }

  return 0;
}

int events_enable(void)
{
  size_t i, j;

  for (i = 0; i < config_threads_num; i++) {
    for (j = 0; j < config_counter_num; j++) {
      event_enable(tid_groups[i].descs[j]);
    }
  }

  return 0;
}

unsigned events_poll(void)
{
  unsigned n = 0;
  size_t i;

  for (i = 0; i < tid_groups_num; i++)
    n += tid_group_poll(tid_groups + i, 32);

  return n;
}
